package kalkulaatortest;

import static org.junit.Assert.*;
import kalkulaator.EvenNumberSum;

import org.junit.Test;

public class EvenNumberSumTest {
	
	@Test
	public void testOneEvenNumberSum() {
		assertEquals("Result is different then expected", 2,
				EvenNumberSum.evenNumberSum(new int[]{2}));
	}
	
	@Test
	public void testFewEvenNumberSum() {
		assertEquals("Result is different then expected", 12,
				EvenNumberSum.evenNumberSum(new int[] {2, 4, 6}));
	}
	
	@Test
	public void testWithNegativeNumbersEvenNumberSum() {
		assertEquals("Result is different then expected", 12,
				EvenNumberSum.evenNumberSum(new int[] {-2, -4, 2, 4, 6}));
	}
	
	@Test
	public void testWithOddNumbersEvenNumberSum() {
		assertEquals("Result is different then expected", 12,
				EvenNumberSum.evenNumberSum(new int[] {1, 2, 4, 6}));
	}
	
	@Test
	public void testWithOddAndNegativeNumbersEvenNumberSum() {
		assertEquals("Result is different then expected", 12,
				EvenNumberSum.evenNumberSum(new int[] {-2, -4, 1, 2, 4, 6}));
	}
}
